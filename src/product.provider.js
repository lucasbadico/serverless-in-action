// import AggregationRoot, { appendCustomMethods, DomainError } from '@spark/dynamodborm'
import {
  region,
  tableName,
  readCapacity,
  writeCapacity,
  indexes,
} from './config'

import schema from './schemas/Product'
import ProductModel from './models/Product'

const DomainName = 'ProductDomain'
function ProductDomainProvider(container) { 
    container.service(DomainName, (c) => {
      const aggregationRoot = new c.AggregationRoot(
        {
          ModelClass: ProductModel,
          tableName,
          region,
          readCapacity,
          writeCapacity,
          indexes,
          className: 'Product',
          schema,
        }
      )

      const {
        Model,
        Product,
        parseFields,
        connection,
        Repository
      } = aggregationRoot
      return {
        Model,
        parseFields,
        connection,
        Repository,
      }
  })
}

ProductDomainProvider.DomainName = DomainName
export default ProductDomainProvider