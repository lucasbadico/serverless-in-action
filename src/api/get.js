import ServiceProvider from '../'

const presenter = ServiceProvider.presenter
const retrieveProduct = ServiceProvider.retrieveProduct

export function handler(event, context, callback) {
  const id = event.pathParams.id
  const options = event.queryParams
  return retrieveProduct(id, options)
    .then(payload => callback(null, presenter.succeeded(payload)))
    .catch(error => callback(presenter.failed(error)))
}

