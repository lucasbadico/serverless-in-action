import ServiceProvider from '../'

const presenter = ServiceProvider.presenter
const createProduct = ServiceProvider.createProduct

export function handler(event, context, callback) {
  const data = event.body
  return createProduct(data)
    .then(payload => callback(null, presenter.succeeded(payload)))
    .catch(error => callback(presenter.failed(error)))
}
