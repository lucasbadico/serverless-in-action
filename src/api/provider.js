import create from './functions/create'
import retrieve from './functions/list'

function ApiProvider(container) { 
   create(container)
   retrieve(container)
}  
export default ApiProvider