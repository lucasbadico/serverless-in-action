const ServiceProvider = require('../').default

const presenter = ServiceProvider.presenter
// const healthCheck = ServiceProvider.healthCheck

export function handler(event, context, callback) {


  return Promise.resolve({ ok: true })
    .then(payload => callback(null, presenter.succeeded(payload)))
    .catch(error => callback(presenter.failed(error)))
}

