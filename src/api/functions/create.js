async function createProduct(
  DynamoDBORM,
  data
) {
  const Product = DynamoDBORM.Product
  const fields =  DynamoDBORM.parseFields(data)
  const product = new Product(fields)

  await product.validate()
  await product.save()

  return product.get(
    'id',
    'description'
  )
}

export function createProductFactory(removeId, AccountDomain) {
  return createProduct.bind(null, removeId, AccountDomain)
}

export function createProductProvider(container) {
  container.service('createProduct', (c) => createProductFactory(c.ProductDomain))
}

export default createProductProvider