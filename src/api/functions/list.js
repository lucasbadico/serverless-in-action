
const fields = [
  'id',
  'description',
  'price'
]
async function listProduct(
  { Repository  },
  id,
  options,
) {
  if (id) {
    const product = await Repository.get({ id, index: 'id-index'}) 
    return product.get(...fields)
  }
  const products = await Repository.find({
    ...options,
    filter: {
      type: 'Equals',
      subject: 'active',
      object: true
    }
  })
  return products.reduce((items, item) => [
    ...items,
    item.get(fields)
  ], [], )
 
}

export function listProductFactory(
  ProductDomain,
) {
  return listProduct.bind(null, toJson, DynamoDBORMError, ProductDomain)
}

export function listProductProvider(container) {
  container.service('retrieveProduct', (c) => listProductFactory(c.ProductDomain))
}

export default listProductProvider
