import v4 from 'uuid/v4'

const schema = hasToBe => ({
    id: {
      type: 'String',
      keyType: 'HASH',
      defaultProvider: v4,
      validator: hasToBe.string().guid()
    },
    price: {
      type: 'String',
      validator: hasToBe.number().min(0.01)
    },
    description: {
      type: 'String',
      validator: hasToBe.string()
    },
    // categories: (embed, embedClass, itemSchema) => ({
    //   type: 'List',
    //   memberType: embed(embedClass),
    //   validator: hasToBe.any().when('kind', {
    //     is: 'customer',
    //     then: hasToBe.array().items(
    //       hasToBe.object().keys(itemSchema)
    //     ).min(1),
    //     otherwise:  hasToBe.array().items(
    //       hasToBe.object().keys(itemSchema)
    //     ).min(0)
    //   })
    // }),
  
  })
  export default schema