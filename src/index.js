const container = require('@spark/services-container')
const {DynamoDBORMProvider} = require('@spark/dynamodborm/lib/provider')
const ProductsDomainProvider = require('./product.provider').default
const ApiProductProvider = require('./api/provider').default

container.service('presenter', () => ({
    succeeded: JSON.stringify,
    failed: (error) => (console.log(error), JSON.stringify(error)),
}))

DynamoDBORMProvider(container)
ProductsDomainProvider(container)
ApiProductProvider(container)

export default container