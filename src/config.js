const region = 'us-east-1'
const tableName = 'products'

const readCapacity = process.env['PRODUCTS_TABLE_READ_CAPACITY'] || 5
const writeCapacity = process.env['PRODUCTS_TABLE_WRITE_CAPACITY'] || 5
const indexes = [
    {
        name: 'id-index',
        readCapacity: process.env['PRODUCTS_TABLE_READ_CAPACITY'] || 5,
        writeCapacity: process.env['PRODUCTS_TABLE_WRITE_CAPACITY'] || 5,
        type: 'global',
        projection: 'all'
    },
    {
        name: 'financialInstitutionId-email-index',
        readCapacity: process.env['PRODUCTS_TABLE_READ_CAPACITY'] || 5,
        writeCapacity: process.env['PRODUCTS_TABLE_WRITE_CAPACITY'] || 5,
        type: 'global',
        projection: 'all'
    }
]

module.exports = {
    region,
    tableName,
    readCapacity,
    writeCapacity,
    indexes,
}