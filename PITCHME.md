@title[Serverless]
# @css[text-orange](`Serverless`)
## @css[text-orange](`Framework`)

por Lucas Badico

+++
## @css[text-orange](`Lucas Badico`) 
Senior Developer | Mentor

+++
## @css[text-orange](`Semeiepontotech`)
a minha missão

+++
## @css[text-orange](`Stefanini`)
Vocês a conhecem?

---
## @css[text-orange](`AWS`)
Como vocês tem usado?

+++
@snap[midpoint]
@fa[question fa-2x]
@snapend

@snap[west span-40 fragment]
@box[bg-white text-black rounded box-padding](Sistemas não critícos e acessórios)
@snapend


@snap[east span-40 fragment]
@box[bg-orange text-white rounded box-padding](Sistemas critícos e centrais)
@snapend

+++

@snap[midpoint]
`De quais serviços estamos falando?`
@snapend

@snap[north-west span-40 fragment]
@box[bg-orange text-white box-padding](ApiGateway)
@snapend

@snap[north-east span-40 fragment]
@box[bg-orange text-white box-padding](Lambda)
@snapend

@snap[south-east span-40 fragment]
@box[bg-orange text-white box-padding](SNS)
@snapend

@snap[south-west span-40 fragment]
@box[bg-orange text-white box-padding](Dynamodb)
@snapend

---
## @css[text-orange](`A api de produtos`) 
Um crud simples de produtos

+++?code=src/api/health-get.js&lang=javascript&color=#272c34
@title[Anatomia da lambda]

@snap[south span-100 text-08]
@[6](Toda lambda terá `event`, `context` e `callback`)
@[8](`event` é o seu request quando trabalhamos com uma api)
@[11](`callback` é o retorno que entregamos pra quem chama a função)
@[12](`callback` recebe dois parametros, o primeiro é o erro)
@[11](o segundo é o sucesso, em caso de sucesso, enviamos o primeiro como `null`)
@snapend


+++
lambda online:

[ieftqfv188.execute-api.us-east-1.amazonaws.com/dev/health](https://ieftqfv188.execute-api.us-east-1.amazonaws.com/dev/health)


---?code=serverless.yml&lang=yaml&color=#272c34
@title[Serverless file]

@snap[south span-100 text-08]
@[18-20](provider, diz com qual cloud estamos trabalhando e a versão do runtime)
@[43-45](functions, tem a nossa função de health)
@[46-50](events, dentro de functions, configuramos o apigateway)
@[100](request)
@[52](response)
@snapend

+++
## @css[text-orange](`Deploy`) 


para ambiente de dev:
```
                 make deploy
```


para ambiente algum outro ambiente:
```
                 make stage=staging deploy
```

---

## @css[text-white](`E o CRUD?`)

@snap[east span-40 fragment]
@box[bg-orange text-white rounded box-padding](Vamos fazer juntos!)
@snapend

---?code=src/api/post.js&lang=javascript&color=#272c34
@[6](Igual a outra lambda)
@[8-10](Nosso serviço de criar produto é chamado)

---?code=src/api/functions/create.js&lang=javascript&color=#272c34
@[1-4](A função que é responsável pelo nosso serviço)
@[2](Esse é a nossa conexão com o banco de dados, injetado via um IoC)
@[3](O data é o que a lambda está injetando na linha 8 do `api/post.js`)

---?code=src/models/Product.js&lang=javascript&color=#272c34

---?code=src/schemas/Product.js&lang=javascript&color=#272c34
[3](hasToBe é uma instancia do `joi`, uma lib de validação do `hapi.js`)
[8](Assim emprestamos a sintaxe do `joi`)
[6](Essa é a nossa chave primária)